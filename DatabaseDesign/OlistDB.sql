create database if not exists `OlistDB`;
use `OlistDB`;

-- Product Category table
drop table if exists product_category_name_translation;
create table product_category_name_translation (	
	product_category_name varchar(100) default null,
	product_category_name_english varchar(100) default null
);

truncate table product_category_name_translation;
load data infile 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\product_category_name_translation.csv' 
into table product_category_name_translation
fields terminated by ','
ignore 1 rows;

drop table if exists product_category;
create table product_category (
	product_category_id int not null primary key auto_increment,
	product_category_name varchar(100) default null,
	product_category_name_english varchar(100) default null
);

truncate table product_category;
insert into product_category (product_category_name, product_category_name_english)
select product_category_name, product_category_name_english from product_category_name_translation;

-- Product table
drop table if exists olist_products_dataset;
create table olist_products_dataset (
	product_id varchar(50) default null,
	product_category_name varchar(100) default null,
	product_name_lenght int default null,
	product_description_lenght int default null,
	product_photos_qty int default null,
	product_weight_g int default null,
	product_length_cm int default null,
	product_height_cm int default null,
	product_width_cm int default null
);

truncate table olist_products_dataset;
load data infile 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\olist_products_dataset.csv' 
into table olist_products_dataset
fields terminated by ','  
enclosed by '"'
ignore 1 rows
   (@product_id, @product_category_name, @product_name_lenght, @product_description_lenght, @product_photos_qty, @product_weight_g, @product_length_cm, @product_height_cm, @product_width_cm) 
        set product_id = nullIF(@product_id, ''), 
			product_category_name = nullIF(@product_category_name, ''), 
			product_name_lenght = nullIF(@product_name_lenght, ''), 
            product_description_lenght = nullIF(@product_description_lenght, ''), 
            product_photos_qty = nullIF(@product_photos_qty, ''),
            product_weight_g = nullIF(@product_weight_g, ''),
            product_length_cm = nullIF(@product_length_cm, ''),
            product_height_cm = nullIF(@product_height_cm, ''),
            product_width_cm = nullIF(@product_width_cm, '');


drop table if exists product;
create table product (
	product_id varchar(50) not null primary key,
	product_category_id int default null,
	product_name_length int default null,
	product_description_length int default null,
	product_photos_qty int default null,
	product_weight_g int default null,
	product_length_cm int default null,
	product_height_cm int default null,
	product_width_cm int default null
);

alter table product
add foreign key (product_category_id) 
references product_category(product_category_id)
on update cascade
on delete cascade;

truncate table product;
insert into product (
	product_id, 
	product_category_id,
	product_name_length,
	product_description_length ,
	product_photos_qty,
	product_weight_g,
	product_length_cm,
	product_height_cm,
	product_width_cm)	
	select product_id, 
	(select product_category_id from product_category where product_category_name=product_category.product_category_name limit 1), 
	product_name_lenght,
	product_description_lenght ,
	product_photos_qty,
	product_weight_g,
	product_length_cm,
	product_height_cm,
	product_width_cm 
	from olist_products_dataset;

-- address table
drop table if exists olist_geolocation_dataset;
create table olist_geolocation_dataset
(	
	geolocation_zip_code_prefix varchar(10),
    geolocation_lat float,
    geolocation_lng float,
	geolocation_city varchar(50) default null,
	geolocation_state varchar(50) default null	 
);

truncate table olist_geolocation_dataset;
load data infile 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\olist_geolocation_dataset.csv' 
into table olist_geolocation_dataset
fields terminated by ','  
enclosed by '"'
ignore 1 rows;
  
-- address table
drop table if exists address;
create table address (	
	zip_code_prefix varchar(10) not null primary key,
	geolocation_city varchar(50) default null,
	geolocation_state varchar(50) default null
);

truncate table address;
insert into address (zip_code_prefix, geolocation_state)
select distinct geolocation_zip_code_prefix, geolocation_state
from olist_geolocation_dataset;

-- ---------------------------------------------
create table olist_customers_dataset (
	customer_id varchar(50),
	customer_unique_id varchar(50) default null,
	customer_zip_code_prefix varchar(10) default null, 
    customer_city varchar(50) default null,
    customer_state varchar(50) default null
);

truncate table olist_customers_dataset;
load data infile 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\olist_customers_dataset.csv' 
into table olist_customers_dataset
fields terminated by ','  
enclosed by '"'
ignore 1 rows;
  

drop table if exists customers;
create table customers (
	customer_id varchar(50) not null primary key,
	customer_unique_id varchar(50) default null,
	zip_code_prefix varchar(10) default null
);

truncate table customers;
insert into customers (customer_id, customer_unique_id, zip_code_prefix)
select customer_id, customer_unique_id, customer_zip_code_prefix 
from olist_customers_dataset;

set SQL_SAFE_UPDATES = 0;
delete from customers 
where zip_code_prefix not in (select zip_code_prefix from address);
set SQL_SAFE_UPDATES = 1;

alter table customers
add foreign key (zip_code_prefix) 
references address(zip_code_prefix) 
on update cascade
on delete cascade;

-- seller table
drop table if exists olist_sellers_dataset;
create table olist_sellers_dataset (
	seller_id varchar(50),
	seller_zip_code_prefix varchar(10) default null, 
    seller_city varchar(50) default null,
    seller_state varchar(50) default null
);

truncate table olist_sellers_dataset;
load data infile 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\olist_sellers_dataset.csv' 
into table olist_sellers_dataset
fields terminated by ','  
enclosed by '"'
ignore 1 rows;

drop table if exists sellers;
create table sellers (
	seller_id varchar(50) not null primary key,
	zip_code_prefix varchar(10) default null
);

truncate table sellers;
insert into sellers (seller_id, zip_code_prefix)
select seller_id, seller_zip_code_prefix 
from olist_sellers_dataset;

set SQL_SAFE_UPDATES = 0;
delete from sellers 
where zip_code_prefix not in (select zip_code_prefix from address);
set SQL_SAFE_UPDATES = 1;

alter table sellers
add foreign key (zip_code_prefix) 
references address(zip_code_prefix)
on update cascade
on delete cascade;

-- ------------------------------------
drop table if exists olist_orders_dataset;
create table olist_orders_dataset (
	order_id varchar(50),
	customer_id varchar(50) default null,
    order_status varchar(20) default null,
	order_purchase_timestamp DATETIME default null,
    order_approved_at DATETIME default null,
	order_delivered_carrier_date DATETIME default null,
    order_delivered_customer_date DATETIME default null,
	order_estimated_delivery_date DATETIME default null	
);

truncate table olist_orders_dataset;
load data infile 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\olist_orders_dataset.csv' 
into table olist_orders_dataset
fields terminated by ','  
enclosed by '"'
ignore 1 rows
   (@order_id, @customer_id, @order_status, @order_purchase_timestamp, @order_approved_at, @order_delivered_carrier_date, @order_delivered_customer_date, @order_estimated_delivery_date) 
        set order_id = nullIF(@order_id, ''),
        customer_id = nullIF(@customer_id, ''), 
        order_status = nullIF(@order_status, ''), 
        order_purchase_timestamp = nullIF(@order_purchase_timestamp, ''), 
		order_approved_at = nullIF(@order_approved_at, ''),
        order_delivered_carrier_date = nullIF(@order_delivered_carrier_date, ''), 
		order_delivered_customer_date = nullIF(@order_delivered_customer_date, ''), 
        order_estimated_delivery_date = nullIF(@order_estimated_delivery_date, '');
            

drop table if exists orders;
create table orders (
	order_id varchar(50) not null primary key,
	customer_id varchar(50) default null,
	order_purchase_timestamp DATETIME default null
);

truncate table orders;
insert into orders (order_id, customer_id, order_purchase_timestamp)
select order_id, customer_id, order_purchase_timestamp
from olist_orders_dataset;

set SQL_SAFE_UPDATES = 0; 
delete from orders 
where customer_id not in (select customer_id from customers);
set SQL_SAFE_UPDATES = 1;

alter table orders
add foreign key (customer_id) 
references customers(customer_id)
on update cascade
on delete cascade;

-- products_ordered table

drop table if exists olist_order_items_dataset;
create table olist_order_items_dataset (
	order_id varchar(50),
    order_item_id varchar(50),
	product_id varchar(50),
	seller_id varchar(50) default null,
    shipping_limit_date datetime,
	price float default null,
	freight_value float default null
);

truncate table olist_order_items_dataset;
load data infile 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\olist_order_items_dataset.csv' 
into table olist_order_items_dataset
fields terminated by ','  
enclosed by '"'
ignore 1 rows;

drop table if exists products_ordered;
create table products_ordered (
	order_id varchar(50) not null,
	product_id varchar(50) not null,
	seller_id varchar(50) default null,
	item_price float default null,
	item_freight_value float default null,
	primary key (order_id, product_id)
);

insert into products_ordered (
	order_id,
	product_id,
	seller_id,
	item_price,
	item_freight_value)
	select distinct
	order_id,
	product_id,
	seller_id,
	price,
	freight_value
from olist_order_items_dataset;

set SQL_SAFE_UPDATES = 0;
delete from products_ordered 
where order_id not in (select order_id from orders);

delete from products_ordered 
where product_id not in (select product_id from product);

delete from products_ordered 
where seller_id not in (select seller_id from sellers);
set SQL_SAFE_UPDATES = 1;

alter table products_ordered
add foreign key (order_id) 
references orders(order_id)
on update cascade
on delete cascade;

alter table products_ordered
add foreign key (product_id) 
references product(product_id)
on update cascade
on delete cascade;

alter table products_ordered
add foreign key (seller_id) 
references sellers(seller_id)
on update cascade
on delete cascade;

-- delivery table

drop table if exists delivery;
create table delivery (
	delivery_id int primary key auto_increment,
	order_id varchar(50) default null,
	order_status varchar(20) default null,
	order_approved_at DATETIME default null,
	order_delivered_carrier_date DATETIME default null,
	order_estimated_delivery_date DATETIME default null,
	order_delivered_customer_date DATETIME default null
);

truncate table delivery;
insert into delivery (
	order_id,
	order_status,
	order_approved_at,
	order_delivered_carrier_date,
	order_estimated_delivery_date,
	order_delivered_customer_date)
	select order_id,
	order_status,
	order_approved_at,
	order_delivered_carrier_date,
	order_estimated_delivery_date,
	order_delivered_customer_date
from olist_orders_dataset;

set SQL_SAFE_UPDATES = 0;
delete from delivery 
where order_id not in (select order_id from orders);
set SQL_SAFE_UPDATES = 1;

alter table delivery
add foreign key (order_id) 
references orders(order_id)
on update cascade
on delete cascade;

-- payment table

drop table if exists olist_order_payments_dataset;
create table olist_order_payments_dataset (	
	order_id varchar(50) default null,
    payment_sequential int default null,
    payment_type varchar(25) default null,
	payment_installments int default null,		
	payment_value float default null
);

truncate table olist_order_payments_dataset;
load data infile 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\olist_order_payments_dataset.csv' 
into table olist_order_payments_dataset
fields terminated by ','  
enclosed by '"'
ignore 1 rows;

drop table if exists payment;
create table payment (
	payment_id int not null primary key auto_increment,
	order_id varchar(50) default null,
	installments int default null,
	sequential int default null,
	payment_type varchar(25) default null,
	transaction_value float default null
);

insert into payment (
	order_id,
	installments,
	sequential,
	payment_type,
	transaction_value)
	select order_id,
	payment_installments,
	payment_sequential,
	payment_type,
	payment_value
from olist_order_payments_dataset;

set SQL_SAFE_UPDATES = 0;
delete from payment 
where order_id not in (select order_id from orders);
set SQL_SAFE_UPDATES = 1;

alter table payment
add foreign key (order_id) 
references orders(order_id)
on update cascade
on delete cascade;

 -- reviews table

drop table olist_order_reviews_dataset;
create table olist_order_reviews_dataset (
	review_id varchar(50) not null,
	order_id varchar(50) not null,
	review_score int not null,
	review_comment_title varchar(100) default null,
	review_comment_message varchar(10000) default null,
	review_creation_date datetime default null,
	review_answer_timestamp datetime default null
);

truncate table olist_order_reviews_dataset;
load data infile 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\olist_order_reviews_dataset.csv' 
into table olist_order_reviews_dataset
fields terminated by ','  
enclosed by '"'
ignore 1 rows
 (@review_id, @order_id, @review_score, @review_comment_title, @review_comment_message, @review_creation_date, @review_answer_timestamp) 
        set review_id = nullIF(@review_id, ''),
        order_id = nullIF(@order_id, ''), 
        review_score = nullIF(@review_score, ''), 
        review_comment_title = nullIF(@review_comment_title, ''), 
        review_comment_message = nullIF(@review_comment_message, ''),
        review_creation_date = nullIF(@review_creation_date, ''), 
		review_answer_timestamp = nullIF(@review_answer_timestamp, '');
          
drop table if exists reviews;
create table reviews (
	review_id varchar(50) not null,
	order_id varchar(50) not null,
	review_score int not null,
	review_comment_title varchar(100) default null,
	review_comment_message varchar(10000) default null,
	review_creation_date DATETIME default null,
	review_answer_timestamp DATETIME default null	,
	primary key (review_id, order_id)
);

insert into reviews (
	review_id,
	order_id,
	review_score,
	review_comment_title,
	review_comment_message,
	review_creation_date,
	review_answer_timestamp)
	select review_id,
	order_id,
	review_score,
	review_comment_title,
	review_comment_message,
	review_creation_date,
	review_answer_timestamp
from olist_order_reviews_dataset;

set SQL_SAFE_UPDATES = 0;
delete from reviews 
where order_id not in (select order_id from orders);
set SQL_SAFE_UPDATES = 1;

alter table reviews
add foreign key (order_id) 
references orders(order_id)
on update cascade
on delete cascade;

