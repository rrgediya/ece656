## Introduction

This project is focused on analysing customer buying trends and predicting potential
purchases based on their current buying behaviour. The dataset used for analysis consists of
over 100,000 orders made at Olist Store in Brazil between 2016 and 2018, across multiple
marketplaces in the country. The dataset contains various dimensions of a particular purchase, including customer information, seller information, product metadata, and customer reviews on their purchase experience. Additionally, the project aims to rate the products sold by the store based on the text in customer reviews.
    The project utilises SQL as the main tool for extracting and manipulating data from
the database, and Plotly as the primary package for visualising query results. The analysis
involves several SQL techniques such as joining tables to combine data from different tables, table manipulation, and aggregating data. The project also includes visualising sales on Olist throughout Brazil.
    Overall, the project aims to gain insights into customer buying behaviour, identify
potential opportunities for business growth and improvements, and provide useful
information to the Olist Store for decision-making purposes.

This dataset was provided by Olist, and downloaded from the link below:
https://www.kaggle.com/datasets/olistbr/brazilian-ecommerce

## Client Requirements

Admin:
● Explore the company’s product volume, sales, and customer satisfaction rating for products.
● Filter and sort options for search results by product category, order status, seller ID
and payment type etc.

Customer:
● Place new order
● Check order status
● Write, update, and delete reviews

Seller:
● Add, update and delete products
● Approve order
