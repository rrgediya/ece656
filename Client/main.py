import pymysql
from read import Read
from update import Update
from create import Create
from delete import Delete

def main():
    print('Admin : A, Customer : C')
    choiceAD = input('Choose your option = ')

    if choiceAD == "A" or choiceAD == 'a':
        print('Available Options: P = List all products for given category')
        choiceop = input('Choose your option = ')

        if choiceop == 'P' or choiceop == 'p':
            value = Read()
            value.func_ReadProducts()

    elif choiceAD == "C" or choiceAD == 'c':
        print('Available Options: N = New order, O = order status, R = Add New Review, U = Update Review, D = Delete Review')
        choiceop = input('Choose your option = ')

        if choiceop == 'U' or choiceop == 'u':
            value = Update()
            value.func_NewReview()
        
        elif choiceop == 'O'or choiceop == 'o':
            value = Read()
            value.func_OrderStatus()   

        elif choiceop == 'N' or choiceop == 'n':
            # sample product_id : 4244733e06e7ecb4970a6e2683c13e61 / 
            # sample zip_code : 11013, 35550, 29830
            value = Create()
            value.func_NewOrder()   
        elif choiceop == 'R'or choiceop == 'r':
            value = Create()
            value.func_NewReview()
        elif choiceop == 'D' or choiceop == 'd':
            value = Delete()
            value.func_DeleteReview()
        else:
            print('Wrong choice, You are going exist.')            
    else:
        print('Wrong choice, Try again')



while 1:
    main()




