import pymysql
import secrets

db = pymysql.connect(host="localhost",
                user="root",
                password="Resu@300498",
                charset="utf8")

cursor = db.cursor(pymysql.cursors.DictCursor)



class Create:
    def func_NewOrder(self):   
        # Get the sql connection
        db = pymysql.connect(host="localhost",
                user="root",
                password="Resu@300498",
                charset="utf8")

        cursor = db.cursor()


        # Ask user input
        # smaple product_id : 4244733e06e7ecb4970a6e2683c13e61
        product_id = input("Enter product_id : ")

        num_ins = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]
        num_seq = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24.25,26,27,28,29]
        while True:
            num = input("Installments (1~24) : ")        

            if num.isdigit() and int(num) in num_ins:
                installment = int(num)
                break    
            else:
                print("Enter Integer number from 1 to 24: ")

        while True:
            num = input("Sequential (1~29) : ")        

            if num.isdigit() and int(num) in num_seq:
                sequential = int(num)
                break    
            else:
                print("Enter Integer number from 1 to 29: ")
        
        type = input("Enter payment option: C: Credit Card, D: Debit Card, B: Boleto, V: Voucher  : ")
        if type == 'C' or type=='c':
            payment_type="credit_card"
        elif type =="D" or type=='d':
            payment_type="debit_card"
        elif type =="B" or type=='b':
            payment_type="boleto"
        elif type =="V" or type=='v':
            payment_type="voucher"
        else:
            print("Wrong input")

        cursor.execute('Use olistdb;')

        # Creating input for orders table
        neworder_id = secrets.token_hex(16)
        customer_id = secrets.token_hex(16)
        unique_customer_id = secrets.token_hex(16)
        

        # Create new Customer
        zip_code_prefix = input("Zipcode : ")
        cursor.execute(f'INSERT INTO customers (customer_id, customer_unique_id, zip_code_prefix) \
                       VALUES("{customer_id}","{unique_customer_id}",{zip_code_prefix});')
        
        
        # Insert new order to orders table
        
        cursor.execute(f'INSERT INTO orders (order_id, customer_id, order_purchase_timestamp) \
                       VALUES("{neworder_id}","{customer_id}",now());')


        # Insert new order to products_ordered table 
        # product_id = input("Enter product_id : ")

        cursor.execute(f'select seller_id \
                        from products_ordered \
                        where product_id = "{product_id}";')

        tempseller_id = cursor.fetchall()
        seller_id = tempseller_id[0][0]

        cursor.execute(f'select item_price \
                        from products_ordered \
                        where product_id = "{product_id}";')
        tempitem_price = cursor.fetchall()
        item_price = tempitem_price[0][0]

        cursor.execute(f'select item_freight_value \
                        from products_ordered \
                        where product_id = "{product_id}";')
        tempitem_freight_value = cursor.fetchall()
        item_freight_value = tempitem_freight_value[0][0]

        cursor.execute(f'INSERT INTO products_ordered (order_id, product_id, seller_id, item_price, item_freight_value) \
                       VALUES("{neworder_id}","{product_id}","{seller_id}",{item_price},{item_freight_value});')
        

        # Insert new order to payment table 
        
        cursor.execute(f'INSERT INTO payment\
                        (payment_id, order_id, installments, sequential, payment_type, transaction_value) \
                       VALUES(default,"{neworder_id}",{installment},{sequential},"{payment_type}",{item_price});')

        # Insert new order to delivery table 
        
        cursor.execute(f'INSERT INTO delivery \
                       (delivery_id, order_id, order_status, order_approved_at, order_delivered_carrier_date,\
                        order_estimated_delivery_date, order_delivered_customer_date) \
                       VALUES(default,"{neworder_id}","processing",now(),null,null,null);')
        
       

        # Print the data

        # value = cursor.fetchall()
        # print(value)
        print("order id :" + neworder_id)
        print("customer_id :" + customer_id)

        db.commit()
        db.close()


    def func_NewReview(self):
        # Get the sql connection
        db = pymysql.connect(host="localhost",
                user="root",
                password="Resu@300498",
                charset="utf8")

        cursor = db.cursor()

        newreview_id = secrets.token_hex(16)
        oid = input("Enter your Order ID : ")
        nums = [1,2,3,4,5]

        while True:
            num = input("Enter the score: ")        

            if num.isdigit() and int(num) in nums:
                score = int(num)
                break    
            else:
                print("Enter Integer number between 1 and 5: ")

        title = input("Title of review : ")
        msg = input("Leave your comment : ")

        # Execute the sql query
        cursor.execute('Use olistdb;')

        cursor.execute(f'INSERT INTO reviews\
                       (review_id, order_id, review_score, review_comment_title, \
                       review_comment_message, review_creation_date, review_answer_timestamp)\
                       VALUES("{newreview_id}","{oid}",{score},"{title}","{msg}",now(),null);')
                        
        
        
        
        # Print the data
        value = cursor.fetchall()
        print("review ID : " + newreview_id)

        db.commit()
        db.close()

